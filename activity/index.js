// PART 1

let username;
let password;
let role;

// PART 2

function loginHere(){
    username = prompt("Enter your username: ").toLowerCase();
    password = prompt("Enter your password: ").toLowerCase();
    role = prompt("Enter your role: ").toLowerCase();

    console.log("username: " + username);
    console.log("password: " + password);
    console.log("role: " + role);

    if(username == false){
        alert("Username must not be empty");
    }
    if(password == false){
        alert("Password must not be empty");
    }
    if(role == false){
        alert("Role must not be empty");
    }

}

loginHere()


if (role === "admin"){
    alert("Welcome back to class portal, admin!")
}
else if (role === "teacher"){
    alert("Thank you for loggin in, teacher!");
}
else if (role === "rookie"){
    alert("Welcome to class portal, student!");
}
else{
    alert("Role out of range.")
}

// PART 3

if(role == "rookie"){

    function checkAverage(grade1, grade2, grade3, grade4){

        let average = (grade1 + grade2 + grade3 + grade4) / 4;

        average = Math.round(average)

        if(average <= 74){
            console.log("Hello, student, your average is " + average + ". The letter equivalent is F");
        }
        else if(average >= 75 && average <= 79){
            console.log("Hello, student, your average is " + average + ". The letter equivalent is D");
        }
        else if(average >= 80 && average <= 84){
            console.log("Hello, student, your average is " + average + ". The letter equivalent is C");
        }
        else if(average >= 85 && average <= 89){
            console.log("Hello, student, your average is " + average + ". The letter equivalent is B");
        }
        else if(average >= 90 && average <= 95){
            console.log("Hello, student, your average is " + average + ". The letter equivalent is A");
        }
        else if(average >= 96){
            console.log("Hello, student, your average is " + average + ". The letter equivalent is A+");
        }
    }
}

else{
    alert(role + "! You are not allowed to access this feature!")
};

