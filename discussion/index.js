console.log("S18 Discussion");

// Conditional Statements
//   if-else
//      if statement = executes if a specified condition is true
        // Syntax
        // if(condition){
        //     code block / statements;
        // }

let numA = 0;

// if(numA < 1){
//     console.log("Hello");
// }

if(numA == 0){
    console.log("Hello")
};

let city = "New York";
if(city == "New York"){
    console.log("Welcome to New York City");
};

// else if clause
//      - executes a statement if previous condition/s are false and if the specified condition is true.
//      - the "else if" clause is optional and can be added to capture additional conditions to change the flow of the program.

let numH = 0;
if(numH < 0){
    console.log("Hello");
}
else if(numH > 0){
    console.log("World");
}
else if(numH == 0){
    console.log("The value is zero");
};

// else statement
//      - executes a statement if all other condition are false/
//      - the "else" statement is original and can be added to capture any other result to change the flow of the program

// city : "New York";
if(city === "New York"){
    console.log("Welcome to New York City");
}
else if(city === "Tokyo"){
    console.log("Welcome to Tokyo, Japan");
}
else {
    console.log("City is not included");
};

// let username = "admin";
// let password = "admin1234";

// if(username == "admin" && password == "admin1234"){
//     console.log("Successful login")
// }
// // if(password = "admin1234"){
// //     console.log("Correct password");
// // }
// else {
//     console.log("Wrong username or password")
// }


let message = "No message";
	console.log(message);

	function determineTyphooneIntensity(windSpeed){

		if(windSpeed < 30){
			return "Not a typhoon yet."
		}
		else if(windSpeed <= 61){
			return "Tropical depression detected."
		}
		else if (windSpeed >= 62 && windSpeed <= 88){
			return "Tropical storm detected."
		}
		else if(windSpeed >= 89 && windSpeed <= 117){
			return "Severe Tropical storm deteceted";
		}
		else{
			return "Typhoon detected."
		}
	}

	message = determineTyphooneIntensity(110);
	console.log(message);

	//  console.warn() is a good way to print warnings in our console that could help us developers act on certain output within our code.
	if(message === "Severe Tropical storm deteceted"){
		console.warn(message);
	}

// Truthy and False
//      - in javascript "truthy" value that is considered true when encountered in a boolean context
//      - false
//          1. false
//          2. 0
//          3. null
//          4.""
//          5. undefined e.g. let number;
//          6. NaN (not a number)


// TRUTHY

if(true){
    console.log("Truthy");
}
if(1){
    console.log("Truthy");
}
if([]){
    console.log("Truthy");
}

// FALSY

// if(false){
//     console.log("Falsy");
// }
// if(0){
//     console.log("Falsy");
// }
// if(null){
//     console.log("Falsy");
// }
// if(""){
//     console.log("Falsy");
// }
// if(NaN){
//     console.log("Falsy");
// }

let isMarried = true;
if(isMarried){
    console.log("Truthy")
}

// Conditional (Ternary) Operator

// let ternaryResult = (1 < 18)? true:false;
// console.log("Result of Ternary Operator: " + ternaryResult)

// let theName;

// function isOfLegalAge(){
//     theName = "John";
//     return "You are in legal age";
// }

// function underAge(){
//     theName = "Jane";
//     return "You are under age limit";
// }

// let age = parseInt(prompt("What is your age?"));
// console.log(age);

// let legalAge = (age >= 18)? isOfLegalAge(): underAge()
// console.log("Return of Ternary Operator in functions " + legalAge + ", " + theName);

// Switch Statements

// let day = prompt("What day of the week is it today?").toLowerCase();
// console.log(day);

// switch(day){
//     case "monday":
//         console.log("The color of the day is red");
//         break
//     case "tuesday":
//         console.log("The color of the day is orange");
// }


// let age = prompt("Age: ");
// age = parseInt(age)
// switch(age){
//     case age <= 18:
//         console.log("Age is valid");
// }

// let agePrompt = prompt("Age: ");

// let age = parseInt(age);
// switch (true) {
//     case (age < 5):
//         alert("less than five");
//         break;
//     case (age < 9):
//         alert("between 5 and 8");
//         break;
//     case (age < 12):
//         alert("between 9 and 11");
//         break;
//     default:
//         alert("none");
//         break;
// }

// Try-Catch-Finally Statement

let codeErrorMessage = "Code Error";
        
function showIntensityAlert(windSpeed){
    try{
        alerat(determineTyphooneIntensity(windSpeed));
    }
    catch(error){
        // error.message is usded to access the information relating to an error onject
        console.warn(error.message)
    }
    finally{
        alert("Intensity updates will show new alert.")
    }

}

showIntensityAlert(56)

console.log("Sample output after try-catch-finally block")